package com.classpath.ekartapplication.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class ApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
       http.authorizeRequests()
               .antMatchers("/login**", "/oauth2**", "/logout**")
                    .permitAll()
               .antMatchers("/api/userinfo**")
                    .authenticated()
               .and()
                .oauth2Login()
               .redirectionEndpoint()
                .baseUri("/authorization-code/callback");
    }
}
