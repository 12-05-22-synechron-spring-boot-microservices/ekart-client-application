package com.classpath.ekartapplication.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/userinfo")
@RequiredArgsConstructor
public class UserInfoRestController {

    private final OAuth2AuthorizedClientService oAuth2AuthorizedClientService;

    @GetMapping
    public Map<String, String> userInfo(OAuth2AuthenticationToken auth2AuthenticationToken){
        Map<String, String> userInfoResultMap = new LinkedHashMap<>();
        OAuth2AuthorizedClient oAuth2AuthorizedClient = this.oAuth2AuthorizedClientService.loadAuthorizedClient(auth2AuthenticationToken.getAuthorizedClientRegistrationId(),
                auth2AuthenticationToken.getPrincipal().getName());
        OAuth2AccessToken accessToken = oAuth2AuthorizedClient.getAccessToken();

        String accessTokenValue = accessToken.getTokenValue();
        String issuedAt = accessToken.getIssuedAt().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE_TIME);
        String expiresAt = accessToken.getExpiresAt().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE_TIME);
        String scopes = accessToken.getScopes().toString();

        userInfoResultMap.put("access-token", accessTokenValue);
        userInfoResultMap.put("issued-at", issuedAt);
        userInfoResultMap.put("expires-at", expiresAt);
        userInfoResultMap.put("scopes", scopes);
        return userInfoResultMap;
    }
}
